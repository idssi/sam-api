var express = require('express');
var router = express.Router();
var inst = require('../utils/getInstance');
var env = require('../utils/env');

//------------AGGIUNGI LAVORO AL GIRONALE------------------//
router.post('/setLavoro', async function (req, res, next) {
  // Ottengo l'account del nodo
  var accounts = await req.web3.eth.getAccounts();
  // Creo l'istanza del contratto
  var instanceGdl = await inst.getInstance(req.web3, env.Gdl);
  // Variabile per la transazione
  var transaction;
  // set dell'listener sull'evento di avvenuta aggiunta al registro
  instanceGdl.once('hackSetLavoro', {}, (error, event) => {
    // Response della rest
    res.send({
      'status': event.returnValues[0],
      'Description': 'Voce aggiunta al Gdl da Rup',
      'tx': transaction
    });
    req.web3.currentProvider.connection.close();
    return true;
  });
  instanceGdl.methods.setLavoro(req.body.data, req.body.meteo,
    req.body.annotazioni, req.body.operai, req.body.mezzi, req.body.path_allegato).send({
      from: accounts[0],
      gas: "4500000",
      privateFor: [env.DLpubKey, env.RUPpubKey, env.AApubKey, env.HostpubKey]
    }, async (error, transactionHash) => {
      if (error) {
        console.log(error);
      } else {
        transaction = transactionHash;
        console.log(transactionHash);
      }
    });
});


//------------AGGIUNGI NUOVA EMAIL ------------------//
router.post('/setEmail', async function (req, res, next) {
  // Ottengo l'account del nodo
  var accounts = await req.web3.eth.getAccounts();
  // Creo l'istanza del contratto
  var instanceEmail = await inst.getInstance(req.web3, env.Email);
  // Variabile per la transazione
  var transaction;
  // set dell'listener sull'evento di avvenuta aggiunta al registro
  instanceEmail.once('hackSetEmail', {}, (error, event) => {
    // Response della rest
    res.send({
      'status': event.returnValues[0],
      'Description': 'Voce aggiunta a Email da DL',
      'tx': transaction
    });
    req.web3.currentProvider.connection.close();
    return true;
  });

  instanceEmail.methods.setEmail(req.body.data, "Rup",
    req.body.destinatario, req.body.oggetto, req.body.testo).send({
      from: accounts[0],
      gas: "4500000",
      privateFor: [env.DLpubKey, env.RUPpubKey, env.AApubKey, env.HostpubKey]
    }, async (error, transactionHash) => {
      if (error) {
        console.log(error);
      } else {
        transaction = transactionHash;
        console.log(transactionHash);
      }
    });
});

//------------VISUALIZZA Email-----------------
router.get('/getEmail', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.Email);
  const result = await instance.methods.getEmail().call();
  const jsonParse = fjson.parse(result, result.length, ["data","mittente","destinatario","oggetto","testo"]);
  req.web3.currentProvider.connection.close();
  return res.send(jsonParse);
});


//------------FIRMA NUOVO Contratto GDL -----------------
router.post('/setSignGDLInit', async function (req, res, next) {
  // Ottengo l'account del nodo
  var accounts = await req.web3.eth.getAccounts();
  // Creo l'istanza del contratto
  var instanceGdlInit = await inst.getInstance(req.web3, env.GdlInit);

  instanceGdlInit.methods.setsignRupInit().send({
    from: accounts[0],
    gas: "4500000",
    privateFor: [env.DLpubKey, env.RUPpubKey, env.AApubKey, env.HostpubKey]
  }, async (error, transactionHash) => {
    if (error) {
      console.log(error);
    } else {
      transaction = transactionHash;
      console.log(transactionHash);
    }
  });
});



//------------FIRMA NUOVO Contratto LDM -----------------
router.post('/setSignLDMInit', async function (req, res, next) {
  // Ottengo l'account del nodo
  var accounts = await req.web3.eth.getAccounts();
  // Creo l'istanza del contratto
  var instanceLdmInit = await inst.getInstance(req.web3, env.LdmInit);

  instanceLdmInit.methods.setsignRupInit().send({
    from: accounts[0],
    gas: "4500000",
    privateFor: [env.DLpubKey, env.RUPpubKey, env.AApubKey, env.HostpubKey]
  }, async (error, transactionHash) => {
    if (error) {
      console.log(error);
    } else {
      transaction = transactionHash;
      console.log(transactionHash);
    }
  });
});


//------------FIRMA NUOVO Contratto RC -----------------
router.post('/setSignRCInit', async function (req, res, next) {
  // Ottengo l'account del nodo
  var accounts = await req.web3.eth.getAccounts();
  // Creo l'istanza del contratto
  var instanceRcInit = await inst.getInstance(req.web3, env.RcInit);

  instanceRcInit.methods.setsignRupInit().send({
    from: accounts[0],
    gas: "4500000",
    privateFor: [env.DLpubKey, env.RUPpubKey, env.AApubKey, env.HostpubKey]
  }, async (error, transactionHash) => {
    if (error) {
      console.log(error);
    } else {
      transaction = transactionHash;
      console.log(transactionHash);
    }
  });
});

//------------INSERISCE TOTALE PROGETTO Contratto SAL -----------------
router.post('/settotalProgetto', async function (req, res, next) {
  // Ottengo l'account del nodo
  var accounts = await req.web3.eth.getAccounts();
  // Creo l'istanza del contratto
  var instanceSALInit = await inst.getInstance(req.web3, env.SalInit);

  instanceSALInit.methods.gettotalProgettoInit().send({
    from: accounts[0],
    gas: "4500000",
    privateFor: [env.DLpubKey, env.RUPpubKey, env.AApubKey, env.HostpubKey]
  }, async (error, transactionHash) => {
    if (error) {
      console.log(error);
    } else {
      transaction = transactionHash;
      console.log(transactionHash);
    }
  });
});

module.exports = router;