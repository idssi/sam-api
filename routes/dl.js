var express = require('express');
var router = express.Router();
var inst = require('../utils/getInstance');
var env = require('../utils/env');
var control = require('../utils/control');
var fjson = require('./../utils/f_json');

//------------AGGIUNGI LAVORO AL GIRONALE------------------//
router.post('/setLavoro', async function (req, res, next) {
  // Ottengo l'account del nodo
  var accounts = await req.web3.eth.getAccounts();
  // Creo l'istanza del contratto
  var instanceGdl = await inst.getInstance(req.web3, env.Gdl);
  // Variabile per la transazione
  var transaction;
  // set dell'listener sull'evento di avvenuta aggiunta al registro
  instanceGdl.once('hackSetLavoro', {}, (error, event) => {
    // Response della rest
    res.send({
      'status': event.returnValues[0],
      'Description': 'Voce aggiunta al Gdl da Ddl',
      'tx': transaction
    });
    req.web3.currentProvider.connection.close();
    return 0;
  });
  instanceGdl.methods.setLavoro(req.body.data, req.body.meteo,
    req.body.annotazioni, req.body.operai, req.body.mezzi, req.body.path_allegato).send({
      from: accounts[0],
      gas: "4500000",
      privateFor: [env.DLpubKey, env.RUPpubKey, env.AApubKey, env.HostpubKey]
    }, async (error, transactionHash) => {
      if (error) {
        console.log(error);
      } else {
        transaction = transactionHash;
        console.log(transactionHash);
      }
    });
});
//------------AGGIUNGI MISURA------------------//
router.post('/setMisura', async function (req, res, next) {
  // ottengo l'account del nodo
  var accounts = await req.web3.eth.getAccounts();
  // creo un'istanza del contratto
  var instance = await inst.getInstance(req.web3, env.Ldm);
  // variabile che conterrà l'hash della transazione
  var transaction;
  // set dell'listener sull'evento di avvenuta aggiunta
  instance.events.hackSetMisura({}, (error, event) => {
    // Response della rest
    res.send({
      'status': event.returnValues[0],
      'Description': 'Misura inserita correttamente',
      'tx': transaction
    });
    req.web3.currentProvider.connection.close();
    return true;
  });
  // set della misura
  instance.methods.setMisura(
    req.body.tar, req.body.data, req.body.des_lav,
    req.body.costo, req.body.aliq, req.body.tot
  ).send({
    from: accounts[0],
    gas: "4500000",
    privateFor: [env.RUPpubKey, env.AApubKey, env.HostpubKey, env.DLpubKey]
  }, async (error, transactionHash) => {
    if (error) {
      // in caso di errore
      console.log(error);
      return res.send(500);
    } else {
      // set della variabile di hash della transazione
      transaction = transactionHash;
      console.log(transactionHash);
    }
  });
});

//------------APPROVA REGISTRO------------------//
router.post('/approvaRegistroDDL', async function (req, res, next) {
  // ottengo l'account del nodo
  var accounts = await req.web3.eth.getAccounts();
  // creo un'istanza del contratto
  var instanceRc = await inst.getInstance(req.web3, env.Rc);
  // creo un'istanza del contratto
  var instanceSal = await inst.getInstance(req.web3, env.Sal);
  // Ottengo la voce del registro desiderata
  const regisV = await instanceRc.methods.getRegistroinfo(req.body.index).call();
  //Controllo che l'item sia presente nella struttura
  control.isSet(regisV, res);

  // Controllo che la voce da firmare non sia gia firmata dal ddl
  if (regisV.sign_DDL) {
    req.web3.currentProvider.connection.close();
    return res.send({
      'status': false,
      'Description': 'Firma precedentemente effettuata dal ddl'
    });
  }
  // set dell'listener sull'evento di avvenuta firma
  await instanceRc.once('hackSignDdl', {}, async (error, event) => {
    // Controllo se l'appaltatore ha firmato la voce appena approvata dal dl
    var regisV2 = await instanceRc.methods.getRegistroinfo(req.body.index).call();
    if (regisV2.sign_AA) {
      // Se l'appaltatore e il ddl hanno approvato la voce aggiungiamo al registro contabile
      instanceSal.methods.setSal(
        regisV2.tariffa, regisV2.data, regisV2.indicazione, regisV2.prezzoUnitario,
        regisV2.percentuale, regisV2.aliq, regisV2.debito, 'DDL'
      ).send({
        from: accounts[0],
        gas: "4500000",
        privateFor: [env.RUPpubKey, env.AApubKey, env.HostpubKey, env.DLpubKey]
      }, (error, transactionHash) => {
        if (error) {
          console.log(error);
        } else {
          console.log(transactionHash);
        }
      });
    } else {
      req.web3.currentProvider.connection.close();
      return res.send({
        'status': true,
        'Description': 'Voce non aggiunta al Sal'
      });
    }
  });
  // set dell'listener sull'evento di avvenuta aggiunta al registro
  instanceSal.once('hackSetSal', {}, (error, event) => {
    // Response della rest
    res.send({
      'status': event.returnValues[0],
      'Description': 'Voce aggiunta al Sal'
    });
    req.web3.currentProvider.connection.close();
    return true;
  });

  // Firmo la voce richiesta
  await instanceRc.methods.approvaRegistroDDL(req.body.index).send({
    from: accounts[0],
    gas: "4500000",
    privateFor: [env.RUPpubKey, env.AApubKey, env.HostpubKey, env.DLpubKey]
  }, async (error, transactionHash) => {
    if (error) {
      console.log(error);
      return res.status(500);
    } else {
      transaction = transactionHash;
    }
  });
});
//------------APPROVAZIONE LAVORO------------------//
router.post('/approvaLavoro', async function (req, res, next) {
  // Ottengo l'account del nodo
  var accounts = await req.web3.eth.getAccounts();
  // Creo l'istanza del contratto
  var instanceGdl = await inst.getInstance(req.web3, env.Gdl);
  // Variabile per la transazione
  var transaction;
  // Ottengo la voce del registro desiderata
  var gdlV = await instanceGdl.methods.getLavoroInfo(req.body.ID).call();
  //Controllo che l'item sia presente nella struttura
  control.isSet(gdlV, res);
  // Controllo che la voce da firmare non sia gia firmata dal ddl
  if (gdlV[6]) {
    req.web3.currentProvider.connection.close();
    return res.send({
      'status': false,
      'Description': 'Firma precedentemente effettuata dal ddl'
    });
  }

  // set dell'listener sull'evento di avvenuta aggiunta al registro
  instanceGdl.once('hackSign', {}, (error, event) => {
    // Response della rest
    res.send({
      'status': event.returnValues[0],
      'Description': 'Voce dal Gdl approvata da Ddl',
      'tx': transaction
    });
    req.web3.currentProvider.connection.close();
    return true;
  });
  // Firmo la voce desiderata
  instanceGdl.methods.approvaLavoro(req.body.ID).send({
    from: accounts[0],
    gas: "4500000",
    privateFor: [env.DLpubKey, env.RUPpubKey, env.AApubKey, env.HostpubKey]
  }, (error, transactionHash) => {
    if (error) {
      console.log(error);
    } else {
      transaction = transactionHash;
      console.log(transactionHash);
    }
  });
});
//------------APPROVAZIONE MISURA------------------//
router.post('/signMisuraDdl', async function (req, res, next) {
  // ottengo l'account del nodo
  var accounts = await req.web3.eth.getAccounts();
  // creo un'istanza del contratto
  var instanceLdm = await inst.getInstance(req.web3, env.Ldm);
  // creo un'istanza del contratto
  var instanceRc = await inst.getInstance(req.web3, env.Rc);
  // Ottengo la voce dell'Ldm desiderata
  const misV = await instanceLdm.methods.getMisura(req.body.index).call();
  // Controllo che l'item sia presente nella struttura
  control.isSet(misV, res);

  // Controllo che la voce da firmare non sia gia firmata dal ddl
  if (misV.sign[0]) {
    req.web3.currentProvider.connection.close();
    return res.send({
      'status': false,
      'Description': 'Firma precedentemente effettuata dal ddl'
    });
  }
  // set dell'listener sull'evento di avvenuta firma
  await instanceLdm.once('hackSignDdl', {}, async (error, event) => {
    // Controllo se l'appaltatore ha firmato la voce appena approvata dal dl
    var misV2 = await instanceLdm.methods.getMisura(req.body.index).call();
    console.log(misV2);
    if (misV2.sign[1]) {
      // Calcolo il debito a partire dal costo
      var debito = ((parseFloat(misV2.costo) * parseFloat(misV2.tot)) / 100).toString();
      // Se l'appaltatore e il ddl hanno approvato la voce aggiungiamo al registro contabile
      instanceRc.methods.setRegistro(
        misV2.tar, misV2.data, misV2.des_lav,
        misV2.tot, misV2.costo, misV2.aliq, debito, 'DDL'
      ).send({
        from: accounts[0],
        gas: "4500000",
        privateFor: [env.RUPpubKey, env.AApubKey, env.HostpubKey, env.DLpubKey]
      }, (error, transactionHash) => {
        if (error) {
          console.log(error);
        } else {
          console.log(transactionHash);
        }
      });
    } else {
      req.web3.currentProvider.connection.close();
      return res.send({
        'status': true,
        'Description': 'Voce non aggiunta al Registro'
      });
    }
  });

  // set dell'listener sull'evento di avvenuta aggiunta al registro
  instanceRc.once('hackSetRegistro', {}, (error, event) => {
    // Response della rest
    res.send({
      'status': event.returnValues[0],
      'Description': 'Voce aggiunta al Registro'
    });
    req.web3.currentProvider.connection.close();
  });

  // Firmo la voce richiesta
  await instanceLdm.methods.signMisuraDdl(req.body.index).send({
    from: accounts[0],
    gas: "4500000",
    privateFor: [env.RUPpubKey, env.AApubKey, env.HostpubKey, env.DLpubKey]
  }, async (error, transactionHash) => {
    if (error) {
      console.log(error);
      return res.send(500);
    } else {
      console.log(transactionHash);
    }
  });
});

//------------AGGIUNGI NUOVA EMAIL ------------------//
router.post('/setEmail', async function (req, res, next) {
  // Ottengo l'account del nodo
  var accounts = await req.web3.eth.getAccounts();
  // Creo l'istanza del contratto
  var instanceEmail = await inst.getInstance(req.web3, env.Email);
  // Variabile per la transazione
  var transaction;

  instanceEmail.methods.setEmail(req.body.data, "Direttore",
    req.body.destinatario, req.body.oggetto, req.body.testo).send({
      from: accounts[0],
      gas: "4500000",
      privateFor: [env.DLpubKey, env.RUPpubKey, env.AApubKey, env.HostpubKey]
    }, async (error, transactionHash) => {
      if (error) {
        console.log(error);
      } else {
        transaction = transactionHash;
        console.log(transactionHash);
        // Response della rest
        res.send({
          'Description': 'Voce aggiunta a Email da DL',
          'tx': transaction
        });
      }
    });
});

//------------VISUALIZZA Email-----------------
router.get('/getEmail', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.Email);
  const result = await instance.methods.getEmail().call();
  const jsonParse = fjson.parse(result, result.length, ["data", "mittente", "destinatario", "oggetto", "testo"]);
  req.web3.currentProvider.connection.close();
  return res.send(jsonParse);
});

//------------FIRMA NUOVO Contratto GDL -----------------
router.post('/setSignGDLInit', async function (req, res, next) {
  // Ottengo l'account del nodo
  var accounts = await req.web3.eth.getAccounts();
  // Creo l'istanza del contratto
  var instanceGdlInit = await inst.getInstance(req.web3, env.GdlInit);

  instanceGdlInit.methods.setsignDirettoreInit().send({
    from: accounts[0],
    gas: "4500000",
    privateFor: [env.DLpubKey, env.RUPpubKey, env.AApubKey, env.HostpubKey]
  }, async (error, transactionHash) => {
    if (error) {
      console.log(error);
    } else {
      transaction = transactionHash;
      console.log(transactionHash);
    }
  });
});


module.exports = router;