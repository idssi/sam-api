var express = require('express');
var router = express.Router();
var inst = require('../utils/getInstance');
var env = require('../utils/env');
var control = require('../utils/control')

//------------AGGIUNGI LAVORO AL GIRONALE------------------//
router.post('/setLavoro', async function (req, res, next) {
  // Ottengo l'account del nodo
  var accounts = await req.web3.eth.getAccounts();
  // Creo l'istanza del contratto
  var instanceGdl = await inst.getInstance(req.web3, env.Gdl);
  // Variabile per la transazione
  var transaction;
  // set dell'listener sull'evento di avvenuta aggiunta al registro
  instanceGdl.once('hackSetLavoro', {}, (error, event) => {
    // Response della rest
    res.send({
      'status': event.returnValues[0],
      'Description': 'Voce aggiunta al Gdl da AA',
      'tx': transaction
    });
    req.web3.currentProvider.connection.close();
    return true;
  });
  instanceGdl.methods.setLavoro(req.body.data, req.body.meteo,
    req.body.annotazioni, req.body.operai, req.body.mezzi, req.body.path_allegato).send({
      from: accounts[0],
      gas: "4500000",
      privateFor: [env.DLpubKey, env.RUPpubKey, env.AApubKey, env.HostpubKey]
    }, async (error, transactionHash) => {
      if (error) {
        console.log(error);
      } else {
        transaction = transactionHash;
        console.log(transactionHash);
      }
    });
});

//-----------------APPROVA MISURA-----------------------
router.post('/signMisuraAa', async function (req, res, next) {
  var accounts = await req.web3.eth.getAccounts();
  // creo un'istanza del contratto
  var instanceLdm = await inst.getInstance(req.web3, env.Ldm);
  // creo un'istanza del contratto
  var instanceRc = await inst.getInstance(req.web3, env.Rc);
  // Ottengo la voce dell'Ldm desiderata
  const misV = await instanceLdm.methods.getMisura(req.body.index).call();
  // Controllo che l'item sia presente nella struttura
  control.isSet(misV, res);

  // Controllo che la voce da firmare non sia gia firmata dal ddl
  if (misV.sign[1]) {
    req.web3.currentProvider.connection.close();
    return res.send({
      'status': true,
      'Description': 'Firma precedentemente effettuata dal aa'
    });
  }
  // set dell'listener sull'evento di avvenuta firma
  await instanceLdm.once('hackSignAa', {}, async (error, event) => {
    // Controllo se l'appaltatore ha firmato la voce appena approvata dal dl
    var misV2 = await instanceLdm.methods.getMisura(req.body.index).call();
    if (misV2.sign[0]) {
      // Calcolo il debito a partire dal costo
      var debito = ((parseFloat(misV2.costo) * parseFloat(misV2.tot)) / 100).toString();
      // Se l'appaltatore e il ddl hanno approvato la voce aggiungiamo al registro contabile
      console.log(misV2);
      instanceRc.methods.setRegistro(
        misV2.tar, misV2.data, misV2.des_lav,
        misV2.tot, misV2.costo, misV2.aliq, debito, 'AA'
      ).send({
        from: accounts[0],
        gas: "4500000",
        privateFor: [env.RUPpubKey, env.AApubKey, env.HostpubKey, env.DLpubKey]
      }, (error, transactionHash) => {
        if (error) {
          console.log(error);
        } else {
          console.log(transactionHash);
        }
      });
    } else {
      req.web3.currentProvider.connection.close();
      return res.send({
        'status': true,
        'Description': 'Voce non aggiunta al registro'
      });
    }
  });

  // set dell'listener sull'evento di avvenuta aggiunta al registro
  instanceRc.once('hackSetRegistro', {}, (error, event) => {
    // Response della rest
    res.send({
      'status': event.returnValues[0],
      'Description': 'Voce aggiunta al registro'
    });
    req.web3.currentProvider.connection.close();
    return true;
  });

  // Firmo la voce richiesta
  await instanceLdm.methods.signMisuraAa(req.body.index).send({
    from: accounts[0],
    gas: "4500000",
    privateFor: [env.RUPpubKey, env.AApubKey, env.HostpubKey, env.DLpubKey]
  }, async (error, transactionHash) => {
    if (error) {
      console.log(error);
      return res.send(500);
    } else {
      console.log(transactionHash);
    }
  });
})

//------------APPROVA REGISTRO------------------//
router.post('/approvaRegistroAA', async function (req, res, next) {
  // ottengo l'account del nodo
  var accounts = await req.web3.eth.getAccounts();
  // creo un'istanza del contratto
  var instanceRc = await inst.getInstance(req.web3, env.Rc);
  // creo un'istanza del contratto
  var instanceSal = await inst.getInstance(req.web3, env.Sal);
  // Ottengo la voce del registro desiderata
  const regisV = await instanceRc.methods.getRegistroinfo(req.body.index).call();
  //Controllo che l'item sia presente nella struttura
  control.isSet(regisV, res);

  // Controllo che la voce da firmare non sia gia firmata dal ddl
  if (regisV.sign_AA) {
    req.web3.currentProvider.connection.close();
    return res.send({
      'status': false,
      'Description': 'Firma precedentemente effettuata dal aa'
    })
  }
  // set dell'listener sull'evento di avvenuta firma
  await instanceRc.once('hackSignAa', {}, async (error, event) => {
    // Controllo se l'appaltatore ha firmato la voce appena approvata dal dl
    var regisV2 = await instanceRc.methods.getRegistroinfo(req.body.index).call();
    if (regisV2.sign_DDL) {
      // Se l'appaltatore e il ddl hanno approvato la voce aggiungiamo al registro contabile
      instanceSal.methods.setSal(
        regisV2.tariffa, regisV2.data, regisV2.indicazione, regisV2.prezzoUnitario,
        regisV2.percentuale, regisV2.aliq, regisV2.debito, 'AA'
      ).send({
        from: accounts[0],
        gas: "4500000",
        privateFor: [env.RUPpubKey, env.AApubKey, env.HostpubKey, env.DLpubKey]
      }, (error, transactionHash) => {
        if (error) {
          console.log(error);
        } else {
          console.log(transactionHash);
        }
      });
    } else {
      req.web3.currentProvider.connection.close();
      return res.send({
        'status': true,
        'Description': 'Voce non aggiunta al Sal'
      });
    }
  });
  // set dell'listener sull'evento di avvenuta aggiunta al registro
  instanceSal.once('hackSetSal', {}, (error, event) => {
    // Response della rest
    res.send({
      'status': event.returnValues[0],
      'Description': 'Voce aggiunta al Sal da AA'
    });
    req.web3.currentProvider.connection.close();
    return true;
  });

  // Firmo la voce richiesta
  await instanceRc.methods.approvaRegistroAA(req.body.index).send({
    from: accounts[0],
    gas: "4500000",
    privateFor: [env.RUPpubKey, env.AApubKey, env.HostpubKey, env.DLpubKey]
  }, async (error, transactionHash) => {
    if (error) {
      console.log(error);
      return res.status(500);
    } else {
      transaction = transactionHash;
    }
  });
});

//------------AGGIUNGI NUOVA EMAIL ------------------//
router.post('/setEmail', async function (req, res, next) {
  // Ottengo l'account del nodo
  var accounts = await req.web3.eth.getAccounts();
  // Creo l'istanza del contratto
  var instanceEmail = await inst.getInstance(req.web3, env.Email);
  // Variabile per la transazione
  var transaction;
  // set dell'listener sull'evento di avvenuta aggiunta al registro
  instanceEmail.once('hackSetEmail', {}, (error, event) => {
    // Response della rest
    res.send({
      'status': event.returnValues[0],
      'Description': 'Voce aggiunta a Email da AA',
      'tx': transaction
    });
    req.web3.currentProvider.connection.close();
    return true;
  });

  instanceEmail.methods.setEmail(req.body.data, "Impresa",
    req.body.destinatario, req.body.oggetto, req.body.testo).send({
      from: accounts[0],
      gas: "4500000",
      privateFor: [env.DLpubKey, env.RUPpubKey, env.AApubKey, env.HostpubKey]
    }, async (error, transactionHash) => {
      if (error) {
        console.log(error);
      } else {
        transaction = transactionHash;
        console.log(transactionHash);
      }
    });
});


//------------VISUALIZZA Email-----------------
router.get('/getEmail', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.Email);
  const result = await instance.methods.getEmail().call();
  const jsonParse = fjson.parse(result, result.length, ["data","mittente","destinatario","oggetto","testo"]);
  req.web3.currentProvider.connection.close();
  return res.send(jsonParse);
});


//------------FIRMA NUOVO Contratto GDL -----------------
router.post('/setSignGDLInit', async function (req, res, next) {
  // Ottengo l'account del nodo
  var accounts = await req.web3.eth.getAccounts();
  // Creo l'istanza del contratto
  var instanceGdlInit = await inst.getInstance(req.web3, env.GdlInit);

  instanceGdlInit.methods.setsignImpresaInit().send({
    from: accounts[0],
    gas: "4500000",
    privateFor: [env.DLpubKey, env.RUPpubKey, env.AApubKey, env.HostpubKey]
  }, async (error, transactionHash) => {
    if (error) {
      console.log(error);
    } else {
      transaction = transactionHash;
      console.log(transactionHash);
    }
  });
});




module.exports = router;