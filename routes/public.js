var express = require('express');
var router = express.Router();
var inst = require('../utils/getInstance');
var fjson = require('./../utils/f_json');
var env = require('../utils/env')

//------------VISUALIZZA LIBRETTO DELLE MISURE------------------
router.get('/getAllLdm', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.Ldm);
  const result = await instance.methods.getAllLdm().call();
  const jsonParse = fjson.parse(result, result.length, ["tar","data","des_lav","costo","aliq","tot","sign_ddl", "sign_aa"]);
  req.web3.currentProvider.connection.close();
  return res.send(jsonParse);
});


//-----------VISUALIZZA GIORNALE LAVORI-------------------
router.get('/getGiornale', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.Gdl);
  const result = await instance.methods.getLavori().call();
  const jsonParse = fjson.parse(result, result.length, ["data","meteo","annotazioni","operai","mezzi","path_allegato","approvato", "ID_giornale"]);
  req.web3.currentProvider.connection.close();
  return res.send(jsonParse);
  
});

//------------VISUALIZZA SAL------------------
router.get('/getSal', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.Sal);
  const result = await instance.methods.getSal().call();
  const jsonParse = fjson.parse(result, result.length, ["tariffa","data","indicazione","importoUnitario","quantita","aliq","tot"]);
  req.web3.currentProvider.connection.close();
  return res.send(jsonParse);
});

//------------VISUALIZZA REGISTRO-----------------
router.get('/getRegistri', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.Rc);
  const result = await instance.methods.getRegistri().call();
  const jsonParse = fjson.parse(result, result.length, ["tariffa","data","indicazione","percentuale","prez_unit", "aliq","debito","signAA","signDDL"]);
  req.web3.currentProvider.connection.close();
  return res.send(jsonParse);
});

//------------VISUALIZZA Firma iniziale AA GDL-----------------
router.get('/getSignAAGDLInit', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.GdlInit);
  const result = await instance.methods.getsignImpresaInit().call();
  return res.send(result);
});

//------------VISUALIZZA Firma iniziale DL GDL-----------------
router.get('/getSignDLGDLInit', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.GdlInit);
  const result = await instance.methods.getsignDirettoreInit().call();
  return res.send(result);
});

//------------VISUALIZZA Firma iniziale RUP GDL-----------------
router.get('/getSignRUPGDLInit', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.GdlInit);
  const result = await instance.methods.getsignRupInit().call();
  return res.send(result);
});

//------------VISUALIZZA Firma iniziale RUP LDM-----------------
router.get('/getSignRUPLDMInit', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.LdmInit);
  const result = await instance.methods.getsignRupInit().call();
  return res.send(result);
});

//------------VISUALIZZA Firma iniziale RUP RC-----------------
router.get('/getSignRUPRCInit', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.RcInit);
  const result = await instance.methods.getsignRupInit().call();
  return res.send(result);
});



//------------VISUALIZZA Oggetto iniziale GDL-------------------
router.get('/getOggettoGDLInit', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.GdlInit);
  const result = await instance.methods.getOggettoInit().call();
  return res.send(result);
});

//------------VISUALIZZA Oggetto iniziale LDM-------------------
router.get('/getOggettoLDMInit', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.LdmInit);
  const result = await instance.methods.getOggettoInit().call();
  return res.send(result);
});

//------------VISUALIZZA Oggetto iniziale RC-------------------
router.get('/getOggettoRCInit', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.RcInit);
  const result = await instance.methods.getOggettoInit().call();
  return res.send(result);
});

//------------VISUALIZZA Oggetto iniziale SAL-------------------
router.get('/getOggettoSALInit', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.SalInit);
  const result = await instance.methods.getOggettoInit().call();
  return res.send(result);
});

//------------VISUALIZZA Committente iniziale GDL-------------------
router.get('/getCommittenteGDLInit', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.GdlInit);
  const result = await instance.methods.getCommittenteInit().call();
  return res.send(result);
});

//------------VISUALIZZA Committente iniziale LDM-------------------
router.get('/getCommittenteLDMInit', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.LdmInit);
  const result = await instance.methods.getCommittenteInit().call();
  return res.send(result);
});

//------------VISUALIZZA Committente iniziale RC-------------------
router.get('/getCommittenteRCInit', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.RcInit);
  const result = await instance.methods.getCommittenteInit().call();
  return res.send(result);
});

//------------VISUALIZZA Committente iniziale SAL-------------------
router.get('/getCommittenteSALInit', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.SalInit);
  const result = await instance.methods.getCommittenteInit().call();
  return res.send(result);
});

//------------VISUALIZZA Impresa iniziale GDL-------------------
router.get('/getImpresaGDLInit', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.GdlInit);
  const result = await instance.methods.getImpresaInit().call();
  return res.send(result);
});

//------------VISUALIZZA Impresa iniziale LDM-------------------
router.get('/getImpresaLDMInit', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.LdmInit);
  const result = await instance.methods.getImpresaInit().call();
  return res.send(result);
});

//------------VISUALIZZA Impresa iniziale RC-------------------
router.get('/getImpresaRCInit', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.RcInit);
  const result = await instance.methods.getImpresaInit().call();
  return res.send(result);
});

//------------VISUALIZZA Impresa iniziale SAL-------------------
router.get('/getImpresaSALInit', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.SalInit);
  const result = await instance.methods.getImpresaInit().call();
  return res.send(result);
});

//------------VISUALIZZA Prezzo Totale iniziale SAL--------------
router.get('/gettotalProgettoInit', async function (req, res, next) {
  instance = await inst.getInstance(req.web3, env.SalInit);
  const result = await instance.methods.gettotalProgettoInit().call();
  return res.send(result);
});

module.exports = router;