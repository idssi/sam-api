var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var morgan = require('morgan');
var winston = require('./config/winston');
// Routes
var indexRouter = require('./routes/index');
var rupRouter = require('./routes/rup');
var dlRouter = require('./routes/dl');
var aaRouter = require('./routes/aa');
var pubRouter = require('./routes/public');

//Blockchain connection variable
var Web3 = require('web3');

//Allow Cors policy
var cors = require('cors');

var app = express();

// Aggiunta dei cors
app.use(cors({ origin: "http://localhost:4200" }));

//Auth0
const jwt = require('express-jwt');
const jwksRsa = require('jwks-rsa');
const permit = require('./middlewares/permission');
// Authentication middleware. When used, the
// Access Token must exist and be verified against
// the Auth0 JSON Web Key Set
const checkJwt = jwt({
  // Dynamically provide a signing key
  // based on the kid in the header and 
  // the signing keys provided by the JWKS endpoint.
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://gwynbleidd.eu.auth0.com/.well-known/jwks.json`
  }),

  // Validate the audience and the issuer.
  audience: 'http://localhost:3000',
  issuer: `https://gwynbleidd.eu.auth0.com/`,
  algorithms: ['RS256']
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(morgan('combined', { stream: winston.stream }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Routing
app.use('/', indexRouter);
app.use('/public',permit.onRole('PUB', Web3, winston), pubRouter);
app.use('/rup', checkJwt, permit.onRole('RUP', Web3, winston), rupRouter);
app.use('/dl', checkJwt, permit.onRole('DL', Web3, winston), dlRouter);
app.use('/aa', checkJwt, permit.onRole('AA', Web3, winston), aaRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // add this line to include winston logging
  winston.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
