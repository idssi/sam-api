// middleware for role-based permissions
module.exports = {
    onRole: function (role, Web3, winston) {
        return (req, res, next) => {
            var port = "";
            var jwtRole
            if (req.user !== undefined) {
                jwtRole = req.user['https://SAMroles/user_authorization'].roles[0]
            } else {
                jwtRole = "PUBLIC";
            }
            if (role !== 'PUB') {
                jwtRole = req.user['https://SAMroles/user_authorization'].roles[0];
                switch (jwtRole) {
                    case 'DL':
                        {
                            winston.info('DL');
                            port = "22000";
                            break;
                        }
                    case 'RUP':
                        {
                            winston.info('RUP');
                            port = "22001";
                            break;
                        }
                    case 'AA':
                        {
                            winston.info('AA');
                            port = "22002";
                            break;
                        }
                }

                if (role === jwtRole) {
                    const provider = new Web3.providers.WebsocketProvider("ws://localhost:" + port);
                    const web3 = new Web3(provider);
                    req.web3 = web3;
                    next(); // role is allowed, so continue on the next middleware
                } else {
                    res.status(403).json({ message: "Forbidden" }); // user is forbidden
                }

            } else {
                winston.info('PUBLIC: ' + jwtRole);
                const provider = new Web3.providers.WebsocketProvider("ws://localhost:22003");
                const web3 = new Web3(provider);
                req.web3 = web3;
                next(); // role is allowed, so continue on the next middleware
            }
        }
    }
}