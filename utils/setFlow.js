var inst = require('./getInstance');
var env = require('./env');

module.exports = {
    setRegistroDdl: async function (web3, misV, res, next) {
        console.log('sono in setRegistroDDL');        // ottengo l'account del nodo
        var accounts = await web3.eth.getAccounts();
        // creo un'istanza del contratto
        var instance = await inst.getInstance(web3, env.Rc);
        // variabile che conterrà l'hash della transazione
        var transaction;
        // Calcolo il debito a partire dal costo
        var debito = ((parseFloat(misV.costo) * parseFloat(misV.tot)) / 100).toString();
        // Se l'appaltatore e il ddl hanno approvato la voce aggiungiamo al registro contabile
        console.log('vado in setRegistroDDl');
        instance.methods.setRegistro(
            misV.tar, misV.data, misV.des_lav,
            misV.tot, misV.costo, misV.aliq, debito
        ).send({
            from: accounts[0],
            gas: "4500000",
            privateFor: [env.RUPpubKey, env.AApubKey, env.HostpubKey, env.DLpubKey]
        }, (error, transactionHash) => {
            if (error) {
                console.log(error);
            } else {
                transaction = transactionHash;
                // set dell'listener sull'evento di avvenuta firma
                instance.once('hackSetRegistro', {}, (error, event) => {
                    console.log('sono in setRegistro evento DDL');
                    // Response della rest
                    res.end('Aggiunta Voce al registro');
                    return next();
                });
            }
        });
    },

    setRegistroAa: async function (web3, misV, res, next) {
        console.log('sono in setRegistroAA');        // ottengo l'account del nodo
        var accounts = await web3.eth.getAccounts();
        // creo un'istanza del contratto
        var instance = await inst.getInstance(web3, env.Rc);
        // variabile che conterrà l'hash della transazione
        var transaction;
        // Calcolo il debito a partire dal costo
        var debito = ((parseFloat(misV.costo) * parseFloat(misV.tot)) / 100).toString();
        // Se l'appaltatore e il ddl hanno approvato la voce aggiungiamo al registro contabile
        console.log('vado in setRegistroAA');
        instance.methods.setRegistro(
            misV.tar, misV.data, misV.des_lav,
            misV.tot, misV.costo, misV.aliq, debito
        ).send({
            from: accounts[0],
            gas: "4500000",
            privateFor: [env.RUPpubKey, env.AApubKey, env.HostpubKey, env.DLpubKey]
        }, (error, transactionHash) => {
            if (error) {
                console.log(error);
            } else {
                transaction = transactionHash;
                // set dell'listener sull'evento di avvenuta firma
                instance.once('hackSetRegistro', {}, (error, event) => {
                    console.log('sono in setRegistro evento AA');
                    // Response della rest
                    res.end('Aggiunta Voce alregistro');
                    return next();
                });
            }
        });
    },

    setSal: async function (web3, regisV) {
        var accounts = await web3.eth.getAccounts();
        // Se l'appaltatore e il ddl hanno approvato la voce aggiungiamo al sal
        var instance = await inst.getInstance(web3, env.Sal);
        instance.methods.setSal(
            regisV.tariffa, regisV.data, regisV.indicazione, regisV.prezzoUnitario,
            regisV.percentuale, regisV.aliq, regisV.debito
        ).send({
            from: accounts[0],
            gas: "4500000",
            privateFor: [env.RUPpubKey, env.AApubKey, env.HostpubKey, env.DLpubKey]
        }, (error, transactionHash) => {
            if (error) {
                console.log(error);
            } else {
                console.log(transactionHash);
            }
        })
    }
}