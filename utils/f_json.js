module.exports = {
    parse: function (values, length, keys) {
        var result = "[";
        for(var i = 0; i < length; i++) {
          result += "{";
          for(var j = 0; j < keys.length; j++) {
            result += "\"" + keys[j] + "\"" + ": " + "\"" + values[i][j] + "\"";
            if(j !== keys.length - 1) {
              result += ", ";
            }
          }
          if(i !== length - 1) {
            result  += "}, ";
          }
          else {
            result  += "}";
          }
        }
        result += "]";
        return result;
      }
    }