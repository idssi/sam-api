module.exports = {
    hackSetMisura: async function (initLength, instance) {
        var length = await instance.methods.getLdmLength().call();
        if(parseInt(initLength) < parseInt(length)) {
            return true
        } else {
            while (parseInt(initLength) == parseInt(length)) {
                length = await instance.methods.getLdmLength().call();
            }
            return true;
        }
    },

    hackSetLavoro: async function (initLength, instance) {
        var length = await instance.methods.countGiornale().call();
        console.log('init: ' + parseInt(initLength) + ' end: ' + parseInt(length))
        if(parseInt(initLength) < parseInt(length)) {
            return true
        } else {
            while (parseInt(initLength) == parseInt(length)) {
                console.log('PRIMA  init: ' + parseInt(initLength) + ' end: ' + parseInt(length));
                length = await instance.methods.countGiornale().call();
                console.log('DOPO  init: ' + parseInt(initLength) + ' end: ' + parseInt(length));
            }
            return true;
        }
    } ,

        hackSetLavoro: async function (initLength, instance) {
        var length = await instance.methods.countGiornale().call();
        console.log('init: ' + parseInt(initLength) + ' end: ' + parseInt(length))
        if(parseInt(initLength) < parseInt(length)) {
            return true
        } else {
            while (parseInt(initLength) == parseInt(length)) {
                console.log('PRIMA  init: ' + parseInt(initLength) + ' end: ' + parseInt(length));
                length = await instance.methods.countGiornale().call();
                console.log('DOPO  init: ' + parseInt(initLength) + ' end: ' + parseInt(length));
            }
            return true;
        }
    } ,

    hackSetEmail: async function (initLength, instance) {
        var length = await instance.methods.countEmail().call();
        console.log('init: ' + parseInt(initLength) + ' end: ' + parseInt(length))
        if(parseInt(initLength) < parseInt(length)) {
            return true
        } else {
            while (parseInt(initLength) == parseInt(length)) {
                console.log('PRIMA  init: ' + parseInt(initLength) + ' end: ' + parseInt(length));
                length = await instance.methods.countGiornale().call();
                console.log('DOPO  init: ' + parseInt(initLength) + ' end: ' + parseInt(length));
            }
            return true;
        }
    } ,

    hackSignMisura: async function (sign, index, instance) {
        var initItem = await instance.methods.getMisura(index).call();
        if(initItem.sign[sign]) {
            return true;
        } else {
            while(!initItem.sign[sign]) {
                initItem = await instance.methods.getMisura(index).call();
            }
            return true;
        }
    },

    hackSignRegistroDDL: async function (index, instance) {
        var initItem = await instance.methods.getRegistroinfo(index).call();
        if(initItem.sign_DDL) {
            return true;
        } else {
            while(!initItem.sign_DDL) {
                initItem = await instance.methods.getRegistroinfo(index).call();
            }
            return true;
        }
    },

    hackSignRegistroAA: async function (index, instance) {
        var initItem = await instance.methods.getRegistroinfo(index).call();
        if(initItem.sign_AA) {
            return true;
        } else {
            while(!initItem.sign_AA) {
                initItem = await instance.methods.getRegistroinfo(index).call();
            }
            return true;
        }
    }


}