module.exports = {
    getInstance: async function (web3, contr) {
        // Get the contract instance.
        const networkId = await web3.eth.net.getId();
        const deployedNetwork = contr.networks[networkId];
        const instance = new web3.eth.Contract(contr.abi, deployedNetwork && deployedNetwork.address);
        return (instance);
    }
}